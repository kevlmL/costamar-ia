package pe.edu.upc.ia.science;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pe.edu.upc.ia.bean.Sale;
import weka.clusterers.SimpleKMeans;
import weka.core.*;
import weka.core.converters.CSVLoader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleKMeansTest {

    @Value("${csv.data.location}")
    private String csvDataLocation;
    private String filename;

    @Before
    public void setUp() {
        filename = "sales.csv";
    }

    @Ignore
    @Test
    public void shouldAddNewSaleAndGetTheCluster() {
        Sale sale = new Sale();
        sale.setBookingDate("10/07/2017 21:45");
        sale.setGds("Kiu");
        sale.setPnr("FPSU2S");
        sale.setEngine("Viajala Peru");
        sale.setStatus("Pending Payment");
        sale.setDepartureDate("04/11/2017 06:00");
        sale.setArrivalDate("08/11/2017 18:00");
        sale.setOrigin("LIM");
        sale.setDestination("AQP");
        sale.setItinerary("LIM-AQP-LIM");
        sale.setPassengersCount(2);
        sale.setContactPerson("KEVIN NOVOA");
        sale.setValidatingAirlines("P9-P9");
        sale.setPaymentType("AMERICAN EXPRESS");
        sale.setAmount("249.90");

        try {
            addNewRecord(csvDataLocation + filename, sale);
            File file = new File(csvDataLocation + filename);

            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setFile(file);
            Instances data = csvLoader.getDataSet();

            SimpleKMeans simpleKMeans = new SimpleKMeans();
            simpleKMeans.setPreserveInstancesOrder(true);
            simpleKMeans.setNumClusters(7);
            simpleKMeans.setSeed(10);
            simpleKMeans.setDontReplaceMissingValues(true);
            simpleKMeans.buildClusterer(data);

            simpleKMeans.setMaxIterations(5000);
            Instances centroids = simpleKMeans.getClusterCentroids();

            for (int i = 0; i < centroids.numInstances(); i++) {
                System.out.println("Centroid " + i + ": " + centroids.instance(i));
            }

            System.out.println("The new instance with pnr: " + sale.getPnr()
                    + " is in cluster " + simpleKMeans.clusterInstance(data.instance(data.numInstances()-1)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addNewRecord(String filename, Sale sale) {
        String newLine = "\n"
                + sale.getBookingDate()
                + "," + sale.getGds()
                + "," + sale.getPnr()
                + "," + sale.getEngine()
                + "," + sale.getStatus()
                + "," + sale.getDepartureDate()
                + "," + sale.getArrivalDate()
                + "," + sale.getOrigin()
                + "," + sale.getDestination()
                + "," + sale.getItinerary()
                + "," + sale.getPassengersCount()
                + "," + sale.getContactPerson()
                + "," + sale.getValidatingAirlines()
                + "," + sale.getPaymentType()
                + "," + sale.getAmount();
        try {
            Files.write(Paths.get(filename), newLine.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
