package pe.edu.upc.ia.bean;

public class Sale {

    private String bookingDate;
    private String gds;
    private String pnr;
    private String engine;
    private String status;
    private String departureDate;
    private String arrivalDate;
    private String origin;
    private String destination;
    private String itinerary;
    private int passengersCount;
    private String contactPerson;
    private String validatingAirlines;
    private String paymentType;
    private String amount;

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getGds() {
        return gds;
    }

    public void setGds(String gds) {
        this.gds = gds;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getItinerary() {
        return itinerary;
    }

    public void setItinerary(String itinerary) {
        this.itinerary = itinerary;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public void setPassengersCount(int passengersCount) {
        this.passengersCount = passengersCount;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getValidatingAirlines() {
        return validatingAirlines;
    }

    public void setValidatingAirlines(String validatingAirlines) {
        this.validatingAirlines = validatingAirlines;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "bookingDate='" + bookingDate + '\'' +
                ", gds='" + gds + '\'' +
                ", pnr='" + pnr + '\'' +
                ", engine='" + engine + '\'' +
                ", status='" + status + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", itinerary='" + itinerary + '\'' +
                ", passengersCount=" + passengersCount +
                ", contactPerson='" + contactPerson + '\'' +
                ", validatingAirlines='" + validatingAirlines + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
