package pe.edu.upc.ia.science;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pe.edu.upc.ia.bean.Sale;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Component
public class ClusterProvider {

    @Value("${csv.data.location}")
    private String csvDataLocation;

    public Integer getClusterFromNewSale(Sale sale) {
        String filename = "sales.csv";
        Integer cluster = null;
        try {
            addNewRecordToBackOffice(csvDataLocation + filename, sale);
            File file = new File(csvDataLocation + filename);

            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setFile(file);
            Instances data = csvLoader.getDataSet();

            SimpleKMeans simpleKMeans = new SimpleKMeans();
            simpleKMeans.setPreserveInstancesOrder(true);
            simpleKMeans.setNumClusters(7);
            simpleKMeans.setSeed(10);
            simpleKMeans.setDontReplaceMissingValues(true);
            simpleKMeans.setMaxIterations(5000);
            simpleKMeans.buildClusterer(data);
            cluster = simpleKMeans.clusterInstance(data.instance(data.numInstances()-1));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cluster;
    }

    private void addNewRecordToBackOffice(String filename, Sale sale) {
        String newLine = "\n"
                + sale.getBookingDate()
                + "," + sale.getGds()
                + "," + sale.getPnr()
                + "," + sale.getEngine()
                + "," + sale.getStatus()
                + "," + sale.getDepartureDate()
                + "," + sale.getArrivalDate()
                + "," + sale.getOrigin()
                + "," + sale.getDestination()
                + "," + sale.getItinerary()
                + "," + sale.getPassengersCount()
                + "," + sale.getContactPerson()
                + "," + sale.getValidatingAirlines()
                + "," + sale.getPaymentType()
                + "," + sale.getAmount();
        try {
            Files.write(Paths.get(filename), newLine.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
