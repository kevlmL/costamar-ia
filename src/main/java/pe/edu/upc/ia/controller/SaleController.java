package pe.edu.upc.ia.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pe.edu.upc.ia.bean.Sale;
import pe.edu.upc.ia.science.ClusterProvider;

@Controller
public class SaleController {

    private final ClusterProvider clusterProvider;

    public SaleController(ClusterProvider clusterProvider) {
        this.clusterProvider = clusterProvider;
    }

    @GetMapping("/sales")
    public String insertSaleForm(Model model) {
        model.addAttribute("sale", new Sale());
        return "sales";
    }

    @PostMapping("/sales")
    public String insertSaleAction(Model model, @ModelAttribute("sale") Sale sale) {
        sale.setContactPerson(sale.getContactPerson().toUpperCase());
        Integer cluster = clusterProvider.getClusterFromNewSale(sale);
        if (cluster != null) {
            model.addAttribute("clusterMessage","La nueva venta fue ingresada en el cluster " + cluster);
            model.addAttribute("sale", new Sale());
        } else {
            model.addAttribute("clusterMessage", "Ocurrio un error al procesar la venta");
        }
        return "sales";
    }
}
