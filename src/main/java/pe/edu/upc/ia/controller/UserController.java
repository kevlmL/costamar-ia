package pe.edu.upc.ia.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pe.edu.upc.ia.bean.User;

@Controller
public class UserController {

    @PostMapping(value = "/login")
    public String login(Model model, @ModelAttribute("user") User user) {
        if (user.getEmail().equalsIgnoreCase("k.novoa@costamar.com")
                && user.getPassword().equalsIgnoreCase("123456")) {
            return "redirect:/sales";
        } else {
            String errorMessage = "usuario o contraseña incorrectos";
            model.addAttribute("user", new User());
            model.addAttribute("errorMessage", errorMessage);
            return "index";
        }
    }
}
