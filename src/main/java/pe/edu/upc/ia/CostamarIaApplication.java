package pe.edu.upc.ia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CostamarIaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CostamarIaApplication.class, args);
	}

}
